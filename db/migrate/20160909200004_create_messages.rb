class CreateMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :messages do |t|
      t.references :user
      t.references :recipient
      t.boolean :is_read, default: false
      t.text :body

      t.timestamps
    end
  end
end

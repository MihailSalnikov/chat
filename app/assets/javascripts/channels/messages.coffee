recepient = window.location.toString().split("/")
recepient = recepient[recepient.length - 1]

App.messages = App.cable.subscriptions.create { channel: "MessagesChannel", recepient_id: recepient },
  connected: ->
    $(".container").css("opacity", "1")
    $("#message_body").prop( "disabled", false );

  disconnected: ->
    $(".container").css("display", "none")
    recepient = null

  received: (data) ->
    $("#messages").append data.message
    $.post "/message/"+data.id.toString()
    return

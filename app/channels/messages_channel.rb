# Be sure to restart your server when you modify this file. Action Cable runs in a loop that does not support auto reloading.
class MessagesChannel < ApplicationCable::Channel

  def subscribed
    # stream_for "#{[current_user.id, params[:recepient_id].to_s].sort.join()}#{Rails.application.secrets.cable_key}"
    stream_for "#{[params[:recepient_id].to_i, current_user.id].sort.join()}#{Rails.application.secrets.cable_key}"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end

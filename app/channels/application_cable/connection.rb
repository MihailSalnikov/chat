module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user, :recepient_id

    def connect
      self.current_user = find_verified_user
      logger.add_tags 'ActionCable', current_user.name
      self.recepient_id = cookies[:recepient_id]
    end

    protected

    def find_verified_user
      verified_user = User.find_by(id: cookies.signed['user.id'])
      if verified_user && cookies.signed['user.expires_at'] > Time.now
        verified_user
      else
        reject_unauthorized_connection
      end
    end

  end
end

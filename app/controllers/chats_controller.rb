class ChatsController < ApplicationController
  before_action :authenticate_user!

  def index
    @users = User.where.not(id: current_user.id)
  end

  def chat
    @messages = Message.where("(user_id = :current_user AND recipient_id = :recipient_id) OR (user_id = :recipient_id AND recipient_id = :current_user)",
                                  current_user: current_user,
                                  recipient_id: params[:id]).order("created_at ASC")
  end

end

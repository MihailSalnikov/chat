class MessagesController < ApplicationController

  def create
    recipient = User.find(params[:id])
    message = current_user.messages.build message_params
    message.recipient = recipient
    message.save
    MessagesChannel.broadcast_to("#{[params[:id].to_i, current_user.id].sort.join()}#{Rails.application.secrets.cable_key}",
      message: render(partial: "messages/message", locals: {message: message}),
      id: message.id)
  end

  def read
    Message.find(params[:id]).read current_user
  end

private

  def message_params
    params.require(:message).permit(:body, :user, :is_read)
  end

end

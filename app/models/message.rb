class Message < ApplicationRecord
  after_create :sms_notification
  belongs_to :user
  belongs_to :recipient, class_name: "User"

  validates :body, :user, :recipient, presence: true


  def read current_user
    if self.recipient_id == current_user.id
      self.is_read = true
      self.save
      $redis.srem("users:sms_posted:id", current_user.id)
    end
  end

private
  def sms_notification 
    SmsNotificationJob.set(WAIT: 5.minute).perform_later self
  end

end

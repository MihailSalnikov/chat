class SmsNotificationJob < ApplicationJob
  queue_as :default

  def perform message
    Message.where(is_read: false, user: message.user, recipient: message.recipient).each do |m|
      if ($redis.sismember("users:sms_posted:id", m.recipient_id.to_s) != true)
        $redis.sadd("users:queue_to_sending:id", m.recipient_id)
      end
    end
    SmsSenderJob.perform_now if $redis.scard("users:queue_to_sending:id") != 0
  end

end


class SmsSenderJob < ApplicationJob
  queue_as :default

  def perform
    id = $redis.spop("users:queue_to_sending:id")
    while id != nil
      puts "SENDING SMS TO USER #{id}"
      $redis.sadd("users:sms_posted:id", id)
      SMSGateway::SMSGateway.send(User.find_by_id(id))
      id = $redis.spop("users:queue_to_sending:id")
    end
  end

end

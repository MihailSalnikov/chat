Rails.application.routes.draw do
  devise_for :users
  root to: 'chats#index'
  get '/chat/:id', to: 'chats#chat', as: 'chat'

  post '/chat/:id', to: 'messages#create', as: 'messages'
  post '/message/:id', to: 'messages#read', as: 'read'
end

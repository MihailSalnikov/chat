FactoryGirl.define do
  sequence :name do |n|
    "user#{n}.#{SecureRandom.hex(3)}"
  end
  factory :user do
    email { "#{generate(:name)}@#{generate(:name)}-company.com" }
    password 'f4k3p455w0rd'
    name { generate(:name) }
    phone 9999999999
  end

  factory :message do
    association :user, factory: :user
    association :recipient, factory: :user
    is_read false
    body "MyText"
  end
end

require 'rails_helper'

RSpec.describe Message, type: :model do
  it { should validate_presence_of(:body) }
  it { should validate_presence_of(:user) }
  it { should validate_presence_of(:recipient) }
  it { should belong_to(:user) }
  it { should belong_to(:recipient).class_name("User") }
end

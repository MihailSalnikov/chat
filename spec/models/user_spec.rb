require 'rails_helper'

RSpec.describe User, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:phone) }
  it { should validate_numericality_of(:phone) }
  it { should have_many(:messages) }
end

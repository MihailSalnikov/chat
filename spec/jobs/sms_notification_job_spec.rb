require 'rails_helper'

RSpec.describe SmsNotificationJob, type: :job do
  describe "SmsNotificationJob tests" do
    before(:all) do
      Message.delete_all
      $redis.del("users:sms_posted:id")
      $redis.del("users:queue_to_sending:id")
    end

    it 'queues the job' do
      expect { SmsNotificationJob.perform_later }
        .to change(ActiveJob::Base.queue_adapter.enqueued_jobs, :size).by(1)
    end

    it 'two different users send messages for two different users' do
      msg = FactoryGirl.create(:message)
      SmsNotificationJob.perform_now msg
      msg2 = FactoryGirl.create(:message)
      SmsNotificationJob.perform_now msg2
      expect($redis.smembers("users:sms_posted:id")).to eq([msg.recipient_id.to_s, msg2.recipient_id.to_s])
    end

    it "two different users send messages for one user" do
      begin_log = File.open("./log.log", 'r'){ |file| file.read }
      user = FactoryGirl.create(:user)
      msg = FactoryGirl.create(:message, recipient: user)
      SmsNotificationJob.perform_now msg
      end_log = begin_log + "#{user.id}\n"
      msg2 = FactoryGirl.create(:message, recipient: user)
      SmsNotificationJob.perform_now msg2
      log = File.open("./log.log", 'r'){ |file| file.read }
      expect(log).to eq(end_log)
    end

    after do
      clear_enqueued_jobs
      clear_performed_jobs
      $redis.del("users:sms_posted:id")
      $redis.del("users:queue_to_sending:id")
    end
  end

end

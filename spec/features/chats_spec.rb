require 'rails_helper'

RSpec.feature "Chats", type: :feature do

  describe "the unlogged users" do
    it "chouse chat" do
      visit '/'
      expect(page).to have_content 'Log in'
    end

    it "chat with user" do
      user = FactoryGirl.create(:user)
      visit '/chat/'+user.id.to_s
      expect(page).to have_content 'Log in'
    end
  end

  describe "the logged users" do
    before :each do
      user = FactoryGirl.create(:user)
      login_as(user, :scope => :user)
    end

    it "chouse chat" do
      another_user = FactoryGirl.create(:user)
      visit '/'
      within("#user_"+another_user.id.to_s) do
        click_link "chat"
      end
      expect(page).to have_content 'chat with '+another_user.name.to_s
    end

    it "send message" do
      another_user = FactoryGirl.create(:user)
      visit "/chat/"+another_user.id.to_s
      within('#chat_form') do
        fill_in 'message[body]', with: 'Hi, it is me'
        click_button 'Send'
      end
      sleep 1
      expect(page).to have_content 'Hi, it is me'
    end
  end

end
